//
//  RepositoryTableViewController.m
//  DesafioIos
//
//  Created by Paulo Rosa on 05/04/17.
//  Copyright © 2017 Paulo Rosa. All rights reserved.
//

#import "RepositoryTableViewController.h"
#import "AFNetworking.h"
#import "Repository.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "DesafioIos-Swift.h"
#import "AppDelegate.h"
#import "Reachability.h"


@interface RepositoryTableViewController ()
{
    NSMutableArray *itens;
    NSURL *baseURL;
    int pageCount;
    BOOL flagReturnForTest;
}
@end

@implementation RepositoryTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getingRepositoryFromAPI];
}

-(void) getingRepositoryFromAPI
{
    //URL for get the repo's
    baseURL = [NSURL URLWithString:@"https://api.github.com/search/repositories"];
    baseURL = [NSURL URLWithString:@"?q=language:Java" relativeToURL:baseURL];
    NSDictionary *params = @{@"sort":@"stars",
                             @"page":@"1"};
    
    if ([self connected]) {
        //HUD for UI
        [SVProgressHUD show];
        itens = [[NSMutableArray alloc]init];
        //getting the repo's
        [self AFNDownloadData:baseURL.absoluteString and:params];
        
        [self.tableView setDragDelegate:self refreshDatePermanentKey:@"Repository"];
        self.tableView.showLoadMoreView = YES;
        pageCount=2;
    }else{
        [self alertViewForConnection];
        [self setPlaceholderForTableviewEmpty];
    }
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.isAccessibilityElement = YES;
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.placeholder.frame = self.tableView.bounds;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)AFNDownloadData:(NSString *)URL and:(NSDictionary *)params
{
    //manager and configs
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    //get method
    [manager GET:URL parameters:params progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
             NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: responseObject options:kNilOptions error:nil];
             NSMutableArray *aux =[NSMutableArray arrayWithArray:[serializedData objectForKey:@"items"]];
             [itens addObjectsFromArray:aux];
             [self.tableView reloadData];
             if ([SVProgressHUD isVisible]) {
                 [SVProgressHUD dismiss];
             }
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
             NSDictionary *serializedDataError = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
             NSLog(@"Failure error serialised - %@",serializedDataError);
         }];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return itens.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Repo" forIndexPath:indexPath];
    cell.isAccessibilityElement = YES;
    if (itens!=nil) {
        NSError *error;
        Repository *repo = [[Repository alloc] initWithDictionary:[itens objectAtIndex:indexPath.row] error:&error];
        
        UILabel * labelTitle = (UILabel *)[cell viewWithTag:1];
        labelTitle.text=repo.nameRepo;
        
        UILabel * labelDesc = (UILabel *)[cell viewWithTag:2];
        labelDesc.text=repo.descRepo;
        
        UILabel * labelForks = (UILabel *)[cell viewWithTag:3];
        labelForks.text=[NSString stringWithFormat:@"%li",(long)repo.forks];
        
        UILabel * labelStars = (UILabel *)[cell viewWithTag:4];
        labelStars.text=[NSString stringWithFormat:@"%li",(long)repo.stars];
        
        UILabel * labelUser = (UILabel *)[cell viewWithTag:6];
        labelUser.text=repo.nameAuthor;
        
        UIImageView * imgAuthor = (UIImageView *)[cell viewWithTag:5];
        [imgAuthor sd_setImageWithURL:[NSURL URLWithString:repo.picture]
                     placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
    }
    
    return cell;
}

#pragma mark - Control datasource

- (void)finishRefresh
{
    
}

- (void)finishLoadMore
{
    if ([self connected]) {
        NSString *intToString = [NSString stringWithFormat:@"%i",pageCount];
        NSDictionary *params = @{@"sort":@"stars",
                                 @"page":intToString};
        //getting the repo's
        [self AFNDownloadData:baseURL.absoluteString and:params];
        [self.tableView finishLoadMore];
        pageCount++;
    }else{
        [self alertViewForConnection];
        [self.tableView finishLoadMore];
    }
}

#pragma mark - Drag delegate methods

- (void)dragTableDidTriggerRefresh:(UITableView *)tableView
{
    [self performSelector:@selector(finishRefresh) withObject:nil afterDelay:2];
}

- (void)dragTableRefreshCanceled:(UITableView *)tableView
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishRefresh) object:nil];
}

- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView
{
    [self performSelector:@selector(finishLoadMore) withObject:nil afterDelay:2];
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishLoadMore) object:nil];
}

-(void)alertViewForConnection
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"No Internet"
                                 message:@"Ops! No internet connection."
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                               }];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)setPlaceholderForTableviewEmpty
{
    UILabel *placeholder = [[UILabel alloc] init];
    placeholder.font = [placeholder.font fontWithSize:20];
    placeholder.numberOfLines = 0;
    placeholder.text = @"Sorry, We can't get your Data";
    placeholder.textAlignment = NSTextAlignmentCenter;
    placeholder.textColor = [UIColor lightGrayColor];
    [self.tableView addSubview:placeholder];
    self.placeholder = placeholder;
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UITableViewCell *cell = sender;
    
    if ([[segue identifier] isEqualToString:@"SEGUE_TO_PULLS"])
    {
        PullTableViewController *vc = [segue destinationViewController];
        UILabel * labelUser = (UILabel *)[cell viewWithTag:6];
        UILabel * labelTitle = (UILabel *)[cell viewWithTag:1];
        [vc recevingDataWithAuthor:labelUser.text and:labelTitle.text];
        
    }
}
@end
