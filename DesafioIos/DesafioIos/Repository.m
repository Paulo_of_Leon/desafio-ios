//
//  Repository.m
//  DesafioIos
//
//  Created by Paulo Rosa on 06/04/17.
//  Copyright © 2017 Paulo Rosa. All rights reserved.
//

#import "Repository.h"

@implementation Repository

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"idRepo": @"id",
                                                                  @"nameRepo": @"name",
                                                                  @"descRepo": @"description",
                                                                  @"nameAuthor": @"owner.login",
                                                                  @"picture": @"owner.avatar_url",
                                                                  @"forks": @"forks_count",
                                                                  @"stars": @"stargazers_count"
                                                                  }];
}


@end
