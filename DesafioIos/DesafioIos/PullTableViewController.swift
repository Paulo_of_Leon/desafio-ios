//
//  PullRequestsTableViewController.swift
//  DesafioIos
//
//  Created by Paulo Rosa on 06/04/17.
//  Copyright © 2017 Paulo Rosa. All rights reserved.

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD
import SDWebImage
import SystemConfiguration


@objc class PullTableViewController: UITableViewController {
    
    var author: String?
    var repo: String?
    var json: JSON = JSON.null
    @IBOutlet weak var placeholder: UILabel!
    var countOpenPull: Int = 0
    var countClosedPull: Int = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem?.accessibilityIdentifier = "back"
        pullRequestsFromAPI()
        
    }
    
    func pullRequestsFromAPI()
    {
        if Reachability.isConnectedToNetwork() == true
        {
            //Internet Connection Available!
            let url: String = "https://api.github.com/repos/" + author! + "/" + repo! + "/pulls"
            self.title = repo
            SVProgressHUD.show()
            alamoGetJSON(url: url)
        }
        else
        {
            //Internet Connection not Available!
            self.noIntertAlert()
            setPlaceholderForTableviewEmpty(text: "Sorry, we can't get your Data")
        }
    }
    
    //for back button in navigation, don't mess with the UX
    override func viewWillDisappear(_ animated: Bool) {
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
    }
    
    func alamoGetJSON(url: String)
    {
        Alamofire.request(url, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                self.json = JSON(value)
                //when no pullrequests found
                if self.json.count != 0 {
                    for pull in self.json.array!{
                        let state = pull["state"].string!
                        
                        if state.caseInsensitiveCompare("open") == ComparisonResult.orderedSame {
                            self.countOpenPull = self.countOpenPull + 1
                        }else{
                            self.countClosedPull = self.countClosedPull + 1
                        }
                    }
                    self.tableView.reloadData()
                }else{
                    self.setPlaceholderForTableviewEmpty(text: "Sorry, no pull requests found")
                }
                if(SVProgressHUD .isVisible()){
                    SVProgressHUD.dismiss()
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func noIntertAlert()
    {
        let alert = UIAlertController(title: "NO INTERNET", message: "Ops! No internet connection.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { action in })
        self.present(alert, animated: true)
    }
    
    public func recevingData(author: String, and repo: String) {
        self.author = author;
        self.repo = repo;
    }
    
    func setPlaceholderForTableviewEmpty(text: String)
    {
        let placeholder = UILabel.init()
        placeholder.font = placeholder.font.withSize(20)
        placeholder.frame.size.height = 42
        placeholder.frame.size.width = tableView.frame.size.width
        placeholder.numberOfLines = 0
        placeholder.text = text
        placeholder.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.tableView.addSubview(placeholder)
        self.placeholder = placeholder
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(self.json.isEmpty){
            return 0
        }else{
            return (self.json.array?.count)! + 1
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell
        
        if (indexPath.row == 0) {
            cell =  tableView.dequeueReusableCell(withIdentifier:"Info", for: indexPath)
            
            let labelOpened = cell.contentView.viewWithTag(10) as? UILabel
            labelOpened?.text =  "\(countOpenPull) opened"
            
            let labelClosed = cell.contentView.viewWithTag(11) as? UILabel
            labelClosed?.text = "\(countClosedPull) closed"
            
        }else{
            cell =  tableView.dequeueReusableCell(withIdentifier:"Requests", for: indexPath)
            
            if(!self.json.isEmpty){
                
                let indexForMatchWithUI = (indexPath.row-1)
                
                let labelTitle = cell.contentView.viewWithTag(1) as? UILabel
                labelTitle?.text = self.json[indexForMatchWithUI]["title"].string
                
                let labelDesc = cell.contentView.viewWithTag(2) as? UILabel
                labelDesc?.text = self.json[indexForMatchWithUI]["body"].string
                
                let labelUser = cell.contentView.viewWithTag(4) as? UILabel
                labelUser?.text = self.json[indexForMatchWithUI]["user"]["login"].string
                
                //converting the date
                let dateString: String = self.json[indexForMatchWithUI]["created_at"].string!
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM, yyyy"
                let date: Date? = dateFormatterGet.date(from: dateString)
                
                let labelDate = cell.contentView.viewWithTag(5) as? UILabel
                labelDate?.text = dateFormatter.string(from: date!)
                
                let imageAuthor = cell.contentView.viewWithTag(3) as? UIImageView
                let stringAux = self.json[indexForMatchWithUI]["user"]["avatar_url"].string
                imageAuthor?.sd_setImage(with: URL(string: stringAux!), placeholderImage: UIImage(named:"user_placeholder"))
                
                
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.row == 0) {
            return 28
        }
        return 134
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DETAIL_PULL_SEGUE"{
            if !SVProgressHUD.isVisible() {
                if let nextViewController = segue.destination as? WebPullRequestViewController{
                    if let indexPath = tableView.indexPathForSelectedRow{
                        var selectedRow = indexPath.row
                        selectedRow = selectedRow-1
                        let url =  self.json[selectedRow]["html_url"].string
                        nextViewController.pageUrl = url!
                        nextViewController.nameRepo = repo
                    }
                }
            }
        }
    }
    
    
}
