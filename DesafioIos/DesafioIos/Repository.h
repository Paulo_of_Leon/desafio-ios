//
//  Repository.h
//  DesafioIos
//
//  Created by Paulo Rosa on 06/04/17.
//  Copyright © 2017 Paulo Rosa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface Repository : JSONModel

@property(nonatomic) NSInteger idRepo;
@property(nonatomic) NSInteger stars;
@property(nonatomic) NSInteger forks;
@property(nonatomic) NSString *nameRepo;
@property(nonatomic) NSString *descRepo;
@property(nonatomic) NSString *nameAuthor;
@property(nonatomic) NSString *picture;
@end

