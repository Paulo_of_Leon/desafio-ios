//
//  AppDelegate.h
//  DesafioIos
//
//  Created by Paulo Rosa on 11/04/17.
//  Copyright © 2017 Paulo Rosa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

