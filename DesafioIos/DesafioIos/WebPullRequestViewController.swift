//
//  WebPullRequestViewController.swift
//  DesafioIos
//
//  Created by Paulo Rosa on 08/04/17.
//  Copyright © 2017 Paulo Rosa. All rights reserved.
//

import UIKit
import SVProgressHUD

class WebPullRequestViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet weak var webview: UIWebView!
    var pageUrl: String?
    var nameRepo: String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        webview.delegate = self
        self.title = "Pull Requests"
        loadURL()
    }
    
    func loadURL()
    {
        if Reachability.isConnectedToNetwork() == true
        {
            if (pageUrl != nil) {
                if let url = URL(string: pageUrl!) {
                    let request = URLRequest(url: url)
                    webview.loadRequest(request)
                }
            }
            
        }
        else
        {
            //Internet Connection not Available!
            self.noIntertAlert()
        }
    }
    
    //for back button in navigation, don't mess with the UX
    override func viewWillDisappear(_ animated: Bool)
    {
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        SVProgressHUD.show()
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        SVProgressHUD.dismiss()
    }
    
    func noIntertAlert()
    {
        let alert = UIAlertController(title: "NO INTERNET", message: "Ops! No internet connection.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { action in })
        self.present(alert, animated: true)
    }
}
