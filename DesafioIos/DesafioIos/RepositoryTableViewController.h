//
//  RepositoryTableViewController.h
//  DesafioIos
//
//  Created by Paulo Rosa on 05/04/17.
//  Copyright © 2017 Paulo Rosa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+DragLoad.h"


@interface RepositoryTableViewController : UITableViewController <UITableViewDragLoadDelegate>
@property (weak, nonatomic) IBOutlet UILabel *placeholder;

//for infinity scroll
- (void)dragTableDidTriggerRefresh:(UITableView *)tableView;
- (void)dragTableRefreshCanceled:(UITableView *)tableView;
- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView;
- (void)dragTableLoadMoreCanceled:(UITableView *)tableView;

@end
